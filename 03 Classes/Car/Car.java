
/**
  A simulated car that consumes gas as it drives.
*/
public class Car
{
  private Picture pic;
  private double milesPerGallon;
  private double milesDriven;
  private double gasInTank;

  /**
    Constructs a car with a given fuel efficiency.
    @param mpg fuel efficiency.
  */
  public Car(double mpg)
  {
    milesPerGallon = mpg;
    gasInTank      = 0;
    milesDriven    = 0;
    pic            = new Picture("car.jpg");

    pic.translate(0, 0);
    pic.draw();
  }

  /**
    Constructs a car with a given fuel efficiency and a custom picture.
    @param mpg fuel efficiency.
    @param pictureFile custom car image.
  */
  public Car(double mpg, String pictureFile)
  {
    milesPerGallon = mpg;
    gasInTank      = 0;
    milesDriven    = 0;
    pic            = new Picture(pictureFile);

    pic.translate(0, 0);
    pic.draw();
  }

  /**
    Increases the value of gas in the car's tank.
    @param amount the gas increment.
  */
  public void addGas(double amount)
  {
    gasInTank = gasInTank + amount;
  }

  /**
    Drives this car by a given distance.
    @param distance the distance to drive
  */
  public void drive(double distance)
  {
    int pixelsPerMile = 10;

    gasInTank   = gasInTank - distance / milesPerGallon;
    milesDriven = milesDriven + distance;
    pic.translate(distance * pixelsPerMile, 0);
  }

  /**
    Gets the current mileage of this car.
    @return the total number of miles driven
  */
  public double getMilesDriven()
  {
    return milesDriven;
  }

  /**
    Gets the current gas amount in the tank.
    @return amount of gas remaining in the tank.
  */
  public double getGasInTank()
  {
    return gasInTank;
  }
}
