
/**
  Simple bank account representation.
*/
public class BankAccount
{
  private double balance;

  /**
    Creates a bank account with the given initial balance.
    @param initialBalance account balance.
  */
  public BankAccount(double initialBalance)
  {
    balance = initialBalance;
  }
}
