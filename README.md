# Intro to Java Programming
Exercises and examples from the course [Intro to Java Programming](https://www.udacity.com/course/intro-to-java-programming--cs046) at [Udacity](https://www.udacity.com).

## Contents
- **Lesson 1 Introduction**: The main function, strings and numbers, printing to console, types of errors in programs.
- **Lesson 2 Objects**: Methods, accessors and mutators, class instantiation, multiple object reference, basic notion of testing, object drawing.
- **Lesson 3 Classes**: Class definition, constructor overload, types in methods, simple use of classes from other files, document strings and documentation generation.

---
- **Problem Set 1**: Running programs and using environment variables.
- **Problem Set 2**: Drawing a tower colorful tower of blocks.
- **Problem Set 3**: Completing and implementing several simple classes.

---
- **Extra 1**: TODO » Improve the `Person` class (lesson 3) using a hash to store friends, allowing it to drawn all friends in a single method.
- **Extra 2**: TODO » Improve the `Flower` class (problem set 3) so the coordinates for the petals and the stem are calculated.

---
May contain code samples from the mentioned source. There are included under _fair use_.
