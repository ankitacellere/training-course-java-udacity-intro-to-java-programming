
public class EnvVariables
{
  public static void main(String[] args) {
    java.util.Properties props = System.getProperties();
    String ide, classpath = "" + props.get("java.class.path");

    System.out.println("OS: " + props.get("os.name") + " " +
      props.get("os.version"));
    System.out.println("Java: " + props.get("java.vendor") + " " +
      props.get("java.version"));

    if (classpath.contains("bluej"))
      ide = "BlueJ";
    else if (props.get("com.horstmann.codecheck") != null)
      ide = "Udacity";
    else
      ide = "Unkown";

    System.out.println("IDE: " + ide);
    System.out.println("Secret code: " +
      Math.abs(ide.hashCode() % 10000));
  }
}
