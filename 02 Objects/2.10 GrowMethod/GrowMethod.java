/**
  Shows the effects of using the `Picture.grow` method.
*/
public class GrowMethod
{
  public static void main(String[] args) {
    Picture planet = new Picture("mars.gif");
    String initialData = planet.toString();

    planet.grow(50, 50);
    planet.draw();

    System.out.println("Planet Initial Data:");
    System.out.println(initialData);
    System.out.println("Planet Ending Data:");
    System.out.println(planet.toString());
  }
}
