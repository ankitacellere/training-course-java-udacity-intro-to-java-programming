
/**
  A simple program to check the use of String#trim.
*/
public class HelloSpace
{
  public static void main(String[] args) {
    String messyString = " Hello, Space! ";
    System.out.println(messyString.trim());
  }
}
