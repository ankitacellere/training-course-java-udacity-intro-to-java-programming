
/**
  Simple program that shows how assignment works for primitives that
  have multiple references.
*/
  public class CopyingNumbers
  {
    public static void main(String[] args)
    {
      int luckyNumber  = 13;
      int luckyNumber2 = luckyNumber;

      luckyNumber2 = 12;
      System.out.println(luckyNumber);
      System.out.println(luckyNumber2);
    }
  }
