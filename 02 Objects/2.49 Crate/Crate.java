
/**
  Simple program that draws a 3D crate.
*/
public class Crate
{
  public static void main(String[] args)
  {
    Rectangle face = new Rectangle(20, 30, 100, 40);
    Line line1 = new Line( 20, 30,  50, 10);
    Line line2 = new Line(120, 70, 150, 50);
    Line line3 = new Line(120, 30, 150, 10);
    Line line4 = new Line( 50, 10, 150, 10);
    Line line5 = new Line(150, 10, 150, 50);

    face.draw();
    line1.draw();
    line2.draw();
    line3.draw();
    line4.draw();
    line5.draw();
  }
}
