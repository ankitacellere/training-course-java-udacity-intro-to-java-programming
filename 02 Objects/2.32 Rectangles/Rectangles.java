
/**
  A simple program that draws two adjacent rectangles.
*/
public class Rectangles
{
  private static final int ORIGIN_X = 60;
  private static final int ORIGIN_Y = 90;
  private static final int WIDTH = 20;
  private static final int HEIGTH = 30;

  public static void main(String[] args) {
    Rectangle a, b;

    a = new Rectangle(ORIGIN_X, ORIGIN_Y, WIDTH, HEIGTH);
    b = new Rectangle(ORIGIN_X + WIDTH, ORIGIN_Y + HEIGTH, WIDTH, HEIGTH);

    a.draw();
    b.draw();
  }
}
