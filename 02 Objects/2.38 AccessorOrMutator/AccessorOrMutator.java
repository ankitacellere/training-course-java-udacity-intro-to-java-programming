
/**
  A simple program to test if a method call access mutates the calling
  object or generates a new one.
*/
public class AccessorOrMutator
{
  public static void main(String[] args) {
    String greeting = "Hello";

    System.out.println(greeting.replace('H', 'J'));
    System.out.println(greeting);
  }
}
