
/**
  Simple program that shows how accessors interact with different
  references to an object.
*/
public class CopyingStrings
{
  public static void main(String[] args)
  {
    String greet1 = "Hello, World!";
    String greet2 = greet1;

    greet2.toUpperCase();
    System.out.println(greet1);
    System.out.println(greet2);
  }
}
