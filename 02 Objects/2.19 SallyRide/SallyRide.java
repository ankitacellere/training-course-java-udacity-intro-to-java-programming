/**
  Gets the number of days a Sally Ride lived having her birth and death
  dates.
*/
public class SallyRide
{
  public static void main(String[] args) {
    Day birth = new Day(1951, 05, 26);
    Day death = new Day(2012, 07, 23);

    System.out.println(death.daysFrom(birth));
  }
}
